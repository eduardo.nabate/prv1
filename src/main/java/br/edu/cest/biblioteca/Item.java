package br.edu.cest.biblioteca;

public class Item {
	String title;
	String publisher;
	String yearpublisher;
	String isbn;
	String price;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public String getYearpublisher() {
		return yearpublisher;
	}
	public void setYearpublisher(String yearpublisher) {
		this.yearpublisher = yearpublisher;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
}
